package com.cmsland.rentcar.controller;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

public class ControllerTest {

    @LocalServerPort
    protected int randomServerPort;

    protected RestTemplate restTemplate = new RestTemplate();

    public URI getFullUri(String servicePath) throws URISyntaxException {
        final String baseUrl = "http://localhost:" + randomServerPort + "/rentcar" + servicePath;
        return new URI(baseUrl);
    }
}
