package com.cmsland.rentcar.controller;

import com.cmsland.rentcar.TestData;
import com.cmsland.rentcar.Util;
import com.cmsland.rentcar.dao.Customer;
import com.cmsland.rentcar.dao.Inventory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.net.URISyntaxException;

import static org.junit.Assert.*;
import static com.cmsland.rentcar.controller.RestfulConst.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InventoryControllerTest extends ControllerTest {

    @Test
    public void getAvailableTest() throws URISyntaxException {
        ResponseEntity<Object[]> result = restTemplate.getForEntity(super.getFullUri(INVENTORY_PATH),
                Object[].class);
        //Verify request succeed
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
        Object[] list = result.getBody();
        for(int i=0; i<list.length; i++)System.out.println(list[i]);
    }

    @Test
    public void getRentListTest() throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(Util.SESSION_HEAD, TestData.WRONG_SESSION);
        HttpEntity entity = new HttpEntity(headers);
        try {
            restTemplate.exchange(super.getFullUri(RENT_CAR_PATH),
                    HttpMethod.GET, entity, Object[].class);
            fail();
        } catch (HttpClientErrorException ex) {
            Assert.assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
        }
        headers = new HttpHeaders();
        HttpEntity<Customer> request = new HttpEntity<>(TestData.getExistUser(), null);
        ResponseEntity<String> result = restTemplate.postForEntity(super.getFullUri(LOGIN_PATH), request, String.class);
        assertNotNull(result.getBody());
        headers.add(Util.SESSION_HEAD, result.getBody());
        entity = new HttpEntity(headers);
        ResponseEntity<Object[]> carResult = restTemplate.exchange(super.getFullUri(RENT_CAR_PATH),
                HttpMethod.GET, entity, Object[].class);
        Object[] list = carResult.getBody();
        if (list.length == 0) {
            HttpEntity<Inventory> car = new HttpEntity<>(TestData.getExistCar(), headers);
            System.out.println(restTemplate.postForObject(super.getFullUri(RENT_CAR_PATH), car, String.class));
            carResult = restTemplate.exchange(super.getFullUri(RENT_CAR_PATH), HttpMethod.GET, entity, Object[].class);
            list = carResult.getBody();
            assertEquals(1, list.length);
            System.out.println(restTemplate.postForObject(super.getFullUri(RETURN_CAR_PATH), car, String.class));
        }
        for(int i=0; i<list.length; i++)System.out.println(list[i]);
    }

    @Test
    public void rentCarTest() throws URISyntaxException {
        Customer user = TestData.getExistUser();
        HttpEntity<Customer> request = new HttpEntity<>(user, null);
        String sessionId = null;
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(super.getFullUri(LOGIN_PATH), request, String.class);
            assertNotNull(result.getBody());
            sessionId = result.getBody();
        } catch (HttpClientErrorException ex) {
            fail();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(Util.SESSION_HEAD, TestData.WRONG_SESSION);
        HttpEntity<Inventory> car = new HttpEntity<>(TestData.getExistCar(), headers);
        try {
            restTemplate.postForObject(super.getFullUri(RENT_CAR_PATH), car, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
        }

        headers = new HttpHeaders();
        headers.add(Util.SESSION_HEAD, sessionId);
        car = new HttpEntity<>(TestData.getWrongCar(), headers);
        try {
            restTemplate.postForObject(super.getFullUri(RENT_CAR_PATH), car, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }

        car = new HttpEntity<>(TestData.getExistCar(), headers);
        try {
            System.out.println(restTemplate.postForObject(super.getFullUri(RENT_CAR_PATH), car, String.class));
        } catch (HttpClientErrorException ex) {
            fail();
        }
        try {
            restTemplate.postForObject(super.getFullUri(RENT_CAR_PATH), car, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE, ex.getStatusCode());
        }
        System.out.println(restTemplate.postForObject(super.getFullUri(RETURN_CAR_PATH), car, String.class));
    }

    @Test
    public void returnCarTest() throws URISyntaxException {
        Customer user = TestData.getExistUser();
        HttpEntity<Customer> request = new HttpEntity<>(user, null);
        String sessionId = null;
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(super.getFullUri(LOGIN_PATH), request, String.class);
            assertNotNull(result.getBody());
            sessionId = result.getBody();
        } catch (HttpClientErrorException ex) {
            fail();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(Util.SESSION_HEAD, TestData.WRONG_SESSION);
        HttpEntity<Inventory> car = new HttpEntity<>(TestData.getExistCar(), headers);
        try {
            restTemplate.postForObject(super.getFullUri(RETURN_CAR_PATH), car, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
        }

        headers = new HttpHeaders();
        headers.add(Util.SESSION_HEAD, sessionId);
        car = new HttpEntity<>(TestData.getWrongCar(), headers);
        try {
            restTemplate.postForObject(super.getFullUri(RETURN_CAR_PATH), car, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }

        car = new HttpEntity<>(TestData.getExistCar(), headers);
        try {
            restTemplate.postForObject(super.getFullUri(RETURN_CAR_PATH), car, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE, ex.getStatusCode());
        }
        try {
            System.out.println(restTemplate.postForObject(super.getFullUri(RENT_CAR_PATH), car, String.class));
            System.out.println(restTemplate.postForObject(super.getFullUri(RETURN_CAR_PATH), car, String.class));
        } catch (HttpClientErrorException ex) {
            fail();
        }
        try {
            restTemplate.postForObject(super.getFullUri(RETURN_CAR_PATH), car, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE, ex.getStatusCode());
        }
    }

}