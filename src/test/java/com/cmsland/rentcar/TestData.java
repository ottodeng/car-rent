package com.cmsland.rentcar;

import com.cmsland.rentcar.dao.Customer;
import com.cmsland.rentcar.dao.Inventory;

public class TestData {
    public static final String WRONG_SESSION = "123";
    public static final String INVALID_USERNAME = "another_user";

    public static Customer getExistUser() {
        return new Customer("tester1", "hello");
    }

    public static Inventory getWrongCar () {
        return new Inventory(1,"null", null);
    }

    public static Inventory getExistCar () {
        return new Inventory(1, "KA123456", "Toyota Camry");
    }
}
