package com.cmsland.rentcar.service;

import com.cmsland.rentcar.TestData;
import com.cmsland.rentcar.dao.Inventory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.ArrayList;

import static com.cmsland.rentcar.TestData.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InventoryServiceTest {

    @Autowired
    InventoryService service;
    @Autowired
    CustomerService customerService;

    @Test
    public void getAvailableTest() {
        ArrayList<Inventory> list = service.getAvailable();
        assertNotNull(list);
        for(int i=0; i<list.size(); i++) System.out.println(list.get(i));
    }

    @Test
    public void getRentByCustomerTest() throws SQLException {
        ArrayList<Inventory> list = service.getRentByCustomer(WRONG_SESSION);
        assertNull(list);
        String session = customerService.login(TestData.getExistUser());
        assertNotNull(session);
        list = service.getRentByCustomer(session);
        if(list.isEmpty()) {
            Inventory existCar = getExistCar();
            assertEquals(RentResult.SUCCESS, service.rentCar(existCar, session));
            list = service.getRentByCustomer(session);
            assertFalse(list.isEmpty());
            assertEquals(ReturnResult.SUCCESS, service.returnCar(existCar, session));
            assertTrue(service.getRentByCustomer(session).isEmpty());
        }
        for(int i=0; i<list.size(); i++) System.out.println(list.get(i));
    }

    @Test
    public void rentAndReturnCarTest() {
        Inventory existCar = getExistCar();
        String session = customerService.login(TestData.getExistUser());
        assertNotNull(session);

        Inventory invalidCar = getWrongCar();
        assertEquals(RentResult.INVALID_INVENTORY, service.rentCar(invalidCar, session));
        assertEquals(RentResult.INVALID_SESSION, service.rentCar(existCar, WRONG_SESSION));
        assertEquals(RentResult.SUCCESS, service.rentCar(existCar, session));
        assertEquals(RentResult.RENTOUT_ALREADY, service.rentCar(existCar, session));

        assertEquals(ReturnResult.INVALID_SESSION, service.returnCar(existCar, WRONG_SESSION));
        assertEquals(ReturnResult.INVALID_INVENTORY, service.returnCar(invalidCar, session));
        assertEquals(ReturnResult.SUCCESS, service.returnCar(existCar, session));
        assertEquals(ReturnResult.RENTBY_MISMATCH, service.returnCar(existCar, session));
    }
}