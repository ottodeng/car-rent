package com.cmsland.rentcar.service;

import com.cmsland.rentcar.TestData;
import com.cmsland.rentcar.dao.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.cmsland.rentcar.TestData.getExistUser;
import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

    @Autowired
    CustomerService service;

    //private static String user = "tester1", password = "hello";

    private static Customer user = TestData.getExistUser();

    @Test
    public void registerTest() {
        assertEquals(0, service.unRegister(user.getName(), user.getPassword()));
        assertEquals(0, service.register(user.getName(), user.getPassword()));
        assertEquals(1, service.register(user.getName(), null));
    }

    @Test
    public void loginAndLogoutTest() {
        String uuid = service.login(user);
        assertNotNull(uuid);
        assertEquals ("empty", service.logout(user.getName(), uuid));
    }

}