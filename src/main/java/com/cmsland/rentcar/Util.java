package com.cmsland.rentcar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Util {

    public final static String SESSION_HEAD = "X-SESSION-ID";
    private static Logger logger = LoggerFactory.getLogger(Util.class);
    private static Connection dbConnection = null;

    public static Connection getDbConnection(){
        if (dbConnection == null) {
            String url = "jdbc:sqlite:" + System.getenv("CARRENT_DBNAME");
            try {
                dbConnection = DriverManager.getConnection(url);
                logger.info("Connected to: " + url);
            }
            catch (SQLException e) {
                logger.error(e.getMessage());
            }
        }
        return dbConnection;
    }

    public static void createNewTable(String dbName) {
        String url = "jdbc:sqlite:" + dbName;
        try (Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement()) {
            String sql = "CREATE TABLE IF NOT EXISTS customer (\n"
                    + "    customer_id integer PRIMARY KEY AUTOINCREMENT,\n"
                    + "    name text NOT NULL,\n"
                    + "    password text NOT NULL,\n"
                    + "    session_id text NULL\n"
                    + "); ";
            stmt.execute(sql);
            sql = "CREATE TABLE IF NOT EXISTS car_model (\n"
                    + "    model_id integer PRIMARY KEY,\n"
                    + "    name text NOT NULL\n"
                    + ");";
            stmt.execute(sql);
            sql = "CREATE TABLE IF NOT EXISTS inventory (\n"
                    + "    inventory_id integer PRIMARY KEY AUTOINCREMENT,\n"
                    + "    model_id integer NOT NULL,\n"
                    + "    engine_no text NOT NULL,\n"
                    + "    rent_by integer NULL,\n"
                    + "    FOREIGN KEY (model_id) REFERENCES car_model (model_id),\n"
                    + "    FOREIGN KEY (rent_by) REFERENCES customer (customer_id)\n"
                    + ");";
            stmt.execute(sql);
            sql = "CREATE TABLE IF NOT EXISTS tenancy (\n"
                    + "    tenancy_id integer PRIMARY KEY AUTOINCREMENT,\n"
                    + "    inventory_id integer NOT NULL,\n"
                    + "    rent_by integer NOT NULL,\n"
                    + "    rent_time text NOT NULL,\n"
                    + "    return_time text NULL,\n"
                    + "    FOREIGN KEY (inventory_id) REFERENCES inventory (inventory_id),\n"
                    + "    FOREIGN KEY (rent_by) REFERENCES customer (customer_id)\n"
                    + ");";
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String initDb() {
        return
              "insert into car_model select 1, 'Toyota Camry'; "
            + "insert into car_model select 2, 'BMW 650';\n"
            + "insert into inventory (model_id, engine_no) select 1, 'KA123456';"
            + "insert into inventory (model_id, engine_no) select 1, 'KA987654';"
            + "insert into inventory (model_id, engine_no) select 2, '1234E56';"
            + "insert into inventory (model_id, engine_no) select 2, '9876E34';";
    }

    public static void main(String[] args) {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String dbName = args == null ? System.getenv("CARRENT_DBNAME") : args[0];
        createNewTable(dbName);
    }

    public static String string2Sha1(String str){
        if(str==null||str.length()==0){
            return null;
        }
        char hexDigits[] = {'0','1','2','3','4','5','6','7','8','9',
                'a','b','c','d','e','f'};
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
            mdTemp.update(str.getBytes("UTF-8"));

            byte[] md = mdTemp.digest();
            int j = md.length;
            char buf[] = new char[j*2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
                buf[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(buf);
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }

}

