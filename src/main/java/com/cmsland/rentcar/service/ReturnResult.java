package com.cmsland.rentcar.service;

public enum ReturnResult {
    SUCCESS,
    INVALID_SESSION,
    DB_ERROR,
    RENTBY_MISMATCH,
    INVALID_INVENTORY
}