package com.cmsland.rentcar.controller;

import com.cmsland.rentcar.Util;
import com.cmsland.rentcar.dao.Inventory;
import com.cmsland.rentcar.service.InventoryService;
import com.cmsland.rentcar.service.RentResult;
import com.cmsland.rentcar.service.ReturnResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

import static com.cmsland.rentcar.controller.RestfulConst.*;

@RestController
public class InventoryController {

    @Autowired
    InventoryService service;

    @GetMapping(path = INVENTORY_PATH, produces = JSON_FORMAT)
    public List<Inventory> getAvailable() {
        return service.getAvailable();
    }

    @GetMapping(path = RENT_CAR_PATH, produces = JSON_FORMAT)
    public ResponseEntity<List<Inventory>> getRentList(
            @RequestHeader(name = Util.SESSION_HEAD, required = true) String session) {
        try {
            List<Inventory> list = service.getRentByCustomer(session);
            if (list == null) return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (SQLException e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = RENT_CAR_PATH, consumes = JSON_FORMAT)
    public ResponseEntity<String> rentCar(@RequestBody Inventory car,
                                         @RequestHeader(name = Util.SESSION_HEAD, required = true) String session) {
        RentResult result = service.rentCar(car, session);
        switch (result) {
            case SUCCESS:
                return new ResponseEntity<>("rent successfully.", HttpStatus.OK);
            case INVALID_SESSION:
                return new ResponseEntity<>("session not found!", HttpStatus.FORBIDDEN);
            case RENTOUT_ALREADY:
                return new ResponseEntity<>("target was rented already!", HttpStatus.NOT_ACCEPTABLE);
            case INVALID_INVENTORY:
                return new ResponseEntity<>("target car is not found!", HttpStatus.NOT_FOUND);
            case DB_ERROR:
                return new ResponseEntity<>("internal db error!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("unknown internal error!", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(path = RETURN_CAR_PATH, consumes = JSON_FORMAT)
    public ResponseEntity<String> returnCar(@RequestBody Inventory car,
                                       @RequestHeader(name = Util.SESSION_HEAD, required = true) String session) {
        ReturnResult result = service.returnCar(car, session);
        switch (result) {
            case SUCCESS:
                return new ResponseEntity<>("return successfully.", HttpStatus.OK);
            case INVALID_SESSION:
                return new ResponseEntity<>("session not found!", HttpStatus.FORBIDDEN);
            case RENTBY_MISMATCH:
                return new ResponseEntity<>("target was rented by other customer!", HttpStatus.NOT_ACCEPTABLE);
            case INVALID_INVENTORY:
                return new ResponseEntity<>("target car is not found!", HttpStatus.NOT_FOUND);
            case DB_ERROR:
                return new ResponseEntity<>("internal db error!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("unknown internal error!", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
