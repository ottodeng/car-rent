package com.cmsland.rentcar.controller;

public class RestfulConst {

    public static final String JSON_FORMAT = "application/json";

    public static final String VERSION_PATH = "/version";

    public static final String DBNAME_PATH = "/dbname";

    public static final String REGISTER_PATH = "/register";

    public static final String LOGIN_PATH = "/login";

    public static final String LOGOUT_PATH = "/logout";

    public static final String INVENTORY_PATH = "/inventory";

    public static final String RENT_CAR_PATH = "/inventory/rent";

    public static final String RETURN_CAR_PATH = "/inventory/return";

}
